import React from 'react'
import { withThemesProvider }from 'themeprovider-storybook'
import light from "../src/theme/light";
import dark from "../src/theme/dark";
import ResetCSS from "../src/ResetCSS";

const globalDecorator = (StoryFn) => (
  <>
  <ResetCSS />
  <StoryFn />
  </>
)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

const themes = [
  {
    name: "Light",
    backgroundColor: "#fff",
    ...light,
  },
  {
    name: "Dark",
    backgroundColor: "#000",
    ...dark,
  },
];

export const decorators = [globalDecorator, withThemesProvider(themes)];