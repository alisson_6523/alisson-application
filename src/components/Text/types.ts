import { LayoutProps, SpaceProps, TypographyProps } from "styled-system";
import { VariantFontSize } from "../../theme/fonts";
import { Variant } from "../../theme/colors";

export interface TextProps extends SpaceProps, TypographyProps, LayoutProps {
    as?: string;
    color?: Variant;
    fontSize?: VariantFontSize;
    bold?: boolean;
    ellipsis?: boolean;
    textTransform?: "uppercase" | "lowercase" | "capitalize";
}
