import React from "react";
import Text from "./Text";
import { baseFonts } from "../../theme/fonts";
import { baseColors } from "../../theme/colors";
import { Meta, ArgTypes } from "@storybook/react";
import { TextProps } from "./types";

export default {
    title: "StyleGuide/Text",
    component: Text,
    argTypes: {
        ref: {
            options: [],
            control: { type: "inline-radio" },
        },
        theme: {
            options: [],
            control: { type: "inline-radio" },
        },
        as: {
            options: [],
            control: { type: "inline-radio" },
        },
        forwardedAs: {
            options: [],
            control: { type: "inline-radio" },
        },
    },
} as Meta;

export const Default: React.FC = () => {
    return (
        <div>
            <Text as="h1">Default</Text>
            <Text bold>Bold text</Text>
            <Text fontSize="md">Custom fontsize</Text>
            <Text color="failure">Custom color</Text>
            <Text>Custom color from theme</Text>
            <Text color="secondary" textTransform="uppercase">
                with text transform
            </Text>
            <Text textAlign="center">center</Text>
            <Text
                display="inline"
                color="primaryDark"
                textTransform="uppercase"
            >
                Example of{" "}
            </Text>
            <Text display="inline" bold textTransform="uppercase">
                inline{" "}
            </Text>
            <Text
                display="inline"
                color="primaryDark"
                textTransform="uppercase"
            >
                Text
            </Text>
            <Text ellipsis width="250px">
                Ellipsis: a long text with an ellipsis just for the example
            </Text>
        </div>
    );
};

export const styleGuide = () => {
    return (
        <>
            <Text as="h1">h1 Lorem Ipsum</Text>
            <Text as="h2">h2 Lorem Ipsum</Text>
            <Text as="h3">h3 Lorem Ipsum</Text>
            <Text as="h4">h4 Lorem Ipsum</Text>
            <Text as="h5">h5 Lorem Ipsum</Text>
            <Text as="h6">h6 Lorem Ipsum</Text>
            <Text as="p" color="secondary">
                *p* Lorem Ipsum
            </Text>
            <Text as="span" color="secondary" fontSize="xss">
                *span* Lorem Ipsum
            </Text>
        </>
    );
};

// export const Primary: ArgTypes = {};

// Primary.args = {
//     children:
//         "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
// } as TextProps;

// Primary.argTypes = {
//     bold: {
//         options: [false, true],
//         control: { type: "inline-radio" },
//     },
//     ellipsis: {
//         options: [false, true],
//         control: { type: "inline-radio" },
//     },

//     fontSize: {
//         options: Object.keys(baseFonts),
//         control: { type: "select" },
//     },

//     color: {
//         options: Object.keys(baseColors).filter((item) => item !== "white"),
//         control: { type: "select" },
//     },

//     textTransform: {
//         options: ["uppercase", "lowercase", "capitalize"],
//         control: { type: "inline-radio" },
//     },

//     textAlign: {
//         options: [
//             "center",
//             "end",
//             "justify",
//             "left",
//             "match-parent",
//             "right",
//             "start",
//         ],
//         control: { type: "inline-radio" },
//     },
// };
