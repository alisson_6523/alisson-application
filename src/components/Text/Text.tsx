import styled, { DefaultTheme, css } from "styled-components";
import { space, typography, layout } from "styled-system";
import { TextProps } from "./types";

interface ThemedProps extends TextProps {
    theme: DefaultTheme;
}

const getColor = (props: ThemedProps) => {
    const { color, theme } = props;

    if (!theme?.colors) return "#000000";

    if (color) return theme?.colors[color];

    return theme?.colors?.primary || "#000";
};

const getFontSize = (props: ThemedProps) => {
    const { fontSize, theme } = props;

    if (!theme?.fonts) return "md";

    if (fontSize) return theme.fonts[fontSize] ?? "md";

    return theme?.fonts?.md ? theme.fonts?.md : "md";
};

const h1 = css<ThemedProps>`
    ${(props) =>
        `${`
            font-style: normal;
            font-size: ${
                props?.fontSize
                    ? getFontSize(props)
                    : props?.theme?.styledGuide?.h1?.fontSize
            };
            color: ${getColor(props)};
            letter-spacing: ${props?.theme?.styledGuide?.h1?.letterSpacing};
            line-height: ${props?.theme?.styledGuide?.h1?.lineHeight};
            ${
                props?.theme?.styledGuide?.h1?.textTransform
                    ? `text-transform: ${props?.theme?.styledGuide?.h1?.textTransform};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h1?.fontFamily
                    ? `font-family: ${props.theme?.styledGuide?.h1?.fontFamily};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h1?.fontWeight
                    ? `font-weight: ${props.theme?.styledGuide?.h1?.fontWeight};`
                    : ""
            }
        `}`};
`;

const h2 = css<ThemedProps>`
    ${(props) =>
        `${`
            font-style: normal;
            font-size: ${
                props?.fontSize
                    ? getFontSize(props)
                    : props?.theme?.styledGuide?.h2?.fontSize
            };
            color: ${getColor(props)};
            letter-spacing: ${props?.theme?.styledGuide?.h2?.letterSpacing};
            line-height: ${props?.theme?.styledGuide?.h2?.lineHeight};
            ${
                props?.theme?.styledGuide?.h2?.textTransform
                    ? `text-transform: ${props?.theme?.styledGuide?.h2?.textTransform};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h2?.fontFamily
                    ? `font-family: ${props.theme?.styledGuide?.h2?.fontFamily};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h2?.fontWeight
                    ? `font-weight: ${props.theme?.styledGuide?.h2?.fontWeight};`
                    : ""
            }

        `}`};
`;

const h3 = css<ThemedProps>`
    ${(props) =>
        `${`
            font-style: normal;
            font-size: ${
                props?.fontSize
                    ? getFontSize(props)
                    : props?.theme?.styledGuide?.h3?.fontSize
            };
            color: ${getColor(props)};
            letter-spacing: ${props?.theme?.styledGuide?.h3?.letterSpacing};
            line-height: ${props?.theme?.styledGuide?.h3?.lineHeight};
            ${
                props?.theme?.styledGuide?.h3?.textTransform
                    ? `text-transform: ${props?.theme?.styledGuide?.h3?.textTransform};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h3?.fontFamily
                    ? `font-family: ${props.theme?.styledGuide?.h3?.fontFamily};`
                    : ""
            }

            ${
                props.theme?.styledGuide?.h3?.fontWeight
                    ? `font-weight: ${props.theme?.styledGuide?.h3?.fontWeight};`
                    : ""
            }
    
        `}`};
`;

const h4 = css<ThemedProps>`
    ${(props) =>
        `${`
            font-style: normal;
            font-size: ${
                props?.fontSize
                    ? getFontSize(props)
                    : props?.theme?.styledGuide?.h4?.fontSize
            };
            color: ${getColor(props)};
            letter-spacing: ${props?.theme?.styledGuide?.h4?.letterSpacing};
            line-height: ${props?.theme?.styledGuide?.h4?.lineHeight};
            ${
                props?.theme?.styledGuide?.h4?.textTransform
                    ? `text-transform: ${props?.theme?.styledGuide?.h4?.textTransform};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h4?.fontFamily
                    ? `font-family: ${props.theme?.styledGuide?.h4?.fontFamily};`
                    : ""
            }

            ${
                props.theme?.styledGuide?.h4?.fontWeight
                    ? `font-weight: ${props.theme?.styledGuide?.h4?.fontWeight};`
                    : ""
            }
            
        `}`};
`;

const h5 = css<ThemedProps>`
    ${(props) =>
        `${`
            font-style: normal;
            font-size: ${
                props?.fontSize
                    ? getFontSize(props)
                    : props?.theme?.styledGuide?.h5?.fontSize
            };
            color: ${getColor(props)};
            letter-spacing: ${props?.theme?.styledGuide?.h5?.letterSpacing};
            line-height: ${props?.theme?.styledGuide?.h5?.lineHeight};
            ${
                props?.theme?.styledGuide?.h5?.textTransform
                    ? `text-transform: ${props?.theme?.styledGuide?.h5?.textTransform};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h5?.fontFamily
                    ? `font-family: ${props.theme?.styledGuide?.h5?.fontFamily};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h5?.fontWeight
                    ? `font-weight: ${props.theme?.styledGuide?.h5?.fontWeight};`
                    : ""
            }
        `}`};
`;

const h6 = css<ThemedProps>`
    ${(props) =>
        `${`
            font-style: normal;
            font-size: ${
                props?.fontSize
                    ? getFontSize(props)
                    : props?.theme?.styledGuide?.h6?.fontSize
            };
            color: ${getColor(props)};
            letter-spacing: ${props?.theme?.styledGuide?.h6?.letterSpacing};
            line-height: ${props?.theme?.styledGuide?.h6?.lineHeight};
            ${
                props?.theme?.styledGuide?.h6?.textTransform
                    ? `text-transform: ${props?.theme?.styledGuide?.h6?.textTransform};`
                    : ""
            }
            ${
                props.theme?.styledGuide?.h6?.fontFamily
                    ? `font-family: ${props.theme?.styledGuide?.h6?.fontFamily};`
                    : ""
            }
            
            ${
                props.theme?.styledGuide?.h6?.fontWeight
                    ? `font-weight: ${props.theme?.styledGuide?.h6?.fontWeight};`
                    : ""
            }
        `}`};
`;

const p = css<ThemedProps>`
    ${(props) =>
        `${`
        font-style: normal;
        font-size: ${
            props?.fontSize
                ? getFontSize(props)
                : props?.theme?.styledGuide?.p?.fontSize
        };
        color: ${getColor(props)};
        letter-spacing: ${props?.theme?.styledGuide?.p?.letterSpacing};
        line-height: ${props?.theme?.styledGuide?.p?.lineHeight};
        ${
            props?.theme?.styledGuide?.p?.textTransform
                ? `text-transform: ${props?.theme?.styledGuide?.p?.textTransform};`
                : ""
        }
        ${
            props.theme?.styledGuide?.p?.fontFamily
                ? `font-family: ${props.theme?.styledGuide?.p?.fontFamily};`
                : ""
        }
        ${
            props.theme?.styledGuide?.p?.fontWeight
                ? `font-weight: ${props.theme?.styledGuide?.p?.fontWeight};`
                : ""
        }
    `}`}
`;

const span = css<ThemedProps>`
    ${(props) =>
        `${`
        font-style: normal;
        font-size: ${
            props?.fontSize
                ? getFontSize(props)
                : props?.theme?.styledGuide?.span?.fontSize
        };
        color: ${getColor(props)};
        letter-spacing: ${props?.theme?.styledGuide?.span?.letterSpacing};
        line-height: ${props?.theme?.styledGuide?.span?.lineHeight};
        
        ${
            props?.theme?.styledGuide?.span?.textTransform
                ? `text-transform: ${props?.theme?.styledGuide?.span?.textTransform};`
                : ""
        }
        ${
            props.theme?.styledGuide?.span?.fontFamily
                ? `font-family: ${props.theme?.styledGuide?.span?.fontFamily};`
                : ""
        }
        ${
            props.theme?.styledGuide?.span?.fontWeight
                ? `font-weight: ${props.theme?.styledGuide?.span?.fontWeight};`
                : ""
        }
    `}`}
`;

const Text = styled.div<TextProps>`
    ${({ textTransform }) =>
        textTransform && `text-transform: ${textTransform};`}
    ${({ ellipsis }) =>
        ellipsis &&
        `white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;`}
    
        

    ${(props) => props.as === "h1" && h1}
    ${(props) => props.as === "h2" && h2}
    ${(props) => props.as === "h3" && h3}
    ${(props) => props.as === "h4" && h4}
    ${(props) => props.as === "h5" && h5}
    ${(props) => props.as === "h6" && h6}
    ${(props) => props.as === "p" && p}
    ${(props) => props.as === "span" && span}

    /* color: ${getColor};
    font-size: ${getFontSize}; */

    ${space}
    ${typography}
    ${layout}
`;

Text.defaultProps = {
    ellipsis: false,
};

export default Text;
