import React from "react";
import { Spiner } from "./types";
import {
    BallTriangle,
    Audio,
    Bars,
    Blocks,
    Circles,
    CirclesWithBar,
    ColorRing,
    Comment,
    Discuss,
    Dna,
    Grid,
    Hearts,
    LineWave,
    MagnifyingGlass,
    Oval,
    ProgressBar,
    Puff,
    Radio,
    RevolvingDot,
    Rings,
    TailSpin,
    ThreeDots,
    ThreeCircles,
    Triangle,
    RotatingTriangles,
    Watch,
    Vortex,
} from "react-loader-spinner";

interface SpinnerProps {
    color: string;
    height: number;
    width: number;
    type: Spiner;
}

export function Spinner(props: SpinnerProps) {
    const { type, ...resto } = props;

    switch (type) {
        case "BallTriangle":
            return <BallTriangle {...resto} />;

        case "Audio":
            return <Audio {...resto} />;
        case "Bars":
            return <Bars {...resto} />;
        case "Blocks":
            return <Blocks {...resto} />;
        case "Circles":
            return <Circles {...resto} />;
        case "CirclesWithBar":
            return <CirclesWithBar {...resto} />;
        case "ColorRing":
            return <ColorRing {...resto} />;
        case "Comment":
            return <Comment {...resto} />;
        case "Discuss":
            return <Discuss {...resto} />;
        case "Dna":
            return <Dna {...resto} />;
        case "Grid":
            return <Grid {...resto} />;
        case "Hearts":
            return <Hearts {...resto} />;
        case "LineWave":
            return <LineWave {...resto} />;
        case "MagnifyingGlass":
            return <MagnifyingGlass {...resto} />;
        case "ProgressBar":
            return <ProgressBar {...resto} />;
        case "Puff":
            return <Puff {...resto} />;
        case "Radio":
            return <Radio {...resto} />;
        case "RevolvingDot":
            return <RevolvingDot {...resto} radius={10} />;
        case "Rings":
            return <Rings {...resto} />;
        case "TailSpin":
            return <TailSpin {...resto} />;
        case "ThreeDots":
            return <ThreeDots {...resto} />;
        case "ThreeCircles":
            return <ThreeCircles {...resto} />;
        case "Triangle":
            return <Triangle {...resto} />;
        case "RotatingTriangles":
            return <RotatingTriangles {...resto} />;
        case "Watch":
            return <Watch {...resto} />;
        case "Vortex":
            return <Vortex {...resto} />;
        default:
            return <Oval {...resto} />;
    }
}
