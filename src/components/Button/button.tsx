import React, { cloneElement, ElementType, isValidElement } from "react";
import { Spinner } from "./spinner";
import { lightColors } from "../../theme/colors";
import getExternalLinkProps from "../../util/getExternalLinkProps";
import StyledButton from "./StyledButton";
import { ButtonProps, scales } from "./types";

const Button = <E extends ElementType = "button">(props: ButtonProps<E>) => {
    const {
        startIcon,
        endIcon,
        external,
        className,
        isLoading,
        disabled,
        children,
        spiner = "BallTriangle",
        heightSpiner = 25,
        widthSpiner = 25,
        spinerColor = "white",
        ...rest
    } = props;

    const internalProps = external ? getExternalLinkProps() : {};
    const isDisabled = isLoading || disabled;
    const classNames = className ? [className] : [];

    if (isLoading) {
        classNames.push("button--loading");
    }

    if (isDisabled && !isLoading) {
        classNames.push("button--disabled");
    }

    function loaderColor() {
        if (spinerColor) return lightColors[spinerColor];
        return "#ECECEC";
    }

    return (
        <StyledButton
            $isLoading={isLoading}
            className={classNames.join(" ")}
            disabled={isDisabled}
            {...internalProps}
            {...rest}
        >
            <>
                {isValidElement(startIcon) && cloneElement(startIcon)}
                {isLoading ? (
                    <Spinner
                        type={spiner}
                        color={loaderColor()}
                        height={heightSpiner}
                        width={widthSpiner}
                    />
                ) : (
                    children
                )}

                {isValidElement(endIcon) && cloneElement(endIcon)}
            </>
        </StyledButton>
    );
};

Button.defaultProps = {
    isLoading: false,
    external: false,
    scale: scales.MD,
    disabled: false,
} as ButtonProps;

export default Button;
