import {
    ComponentProps,
    ElementType,
    ReactElement,
    ReactFragment,
    ReactPortal,
} from "react";
import { Variant } from "../../theme/colors";
import { VariantFontSize } from "../../theme/fonts";
import { Link } from "react-router-dom";
import { LayoutProps, SpaceProps } from "styled-system";
type ReactNode =
    | ReactElement
    | string
    | number
    | ReactFragment
    | ReactPortal
    | boolean
    | null
    | undefined;

export const scales = {
    MD: "md",
    SM: "sm",
    XS: "xs",
} as const;

export const variants = {
    PRIMARY: "primary",
    SECONDARY: "secondary",
    TERTIARY: "tertiary",
    TEXT: "text",
    DANGER: "danger",
    SUBTLE: "subtle",
    SUCCESS: "success",
} as const;

export const spiner = {
    BALLTRIANGLE: "BallTriangle",
    AUDIO: "Audio",
    BARS: "Bars",
    BLOCKS: "Blocks",
    CIRCLES: "Circles",
    CIRCLESWITHBAR: "CirclesWithBar",
    COLORRING: "ColorRing",
    COMMENT: "Comment",
    DISCUSS: "Discuss",
    DNA: "Dna",
    GRID: "Grid",
    FALLINGLINES: "FallingLines",
    HEARTS: "Hearts",
    LINEWAVE: "LineWave",
    MAGNIFYINGGLASS: "MagnifyingGlass",
    OVAL: "Oval",
    PROGRESSBAR: "ProgressBar",
    PUFF: "Puff",
    RADIO: "Radio",
    REVOLVINGDOT: "RevolvingDot",
    ROTATINGSQUARE: "RotatingSquare",
    RINGS: "Rings",
    TAILSPIN: "TailSpin",
    THREEDOTS: "ThreeDots",
    THREECIRCLES: "ThreeCircles",
    TRIANGLE: "Triangle",
    ROTATINGTRIANGLES: "RotatingTriangles",
    WATCH: "Watch",
    VORTEX: "Vortex",
} as const;

export type Scale = typeof scales[keyof typeof scales];

export type Spiner = typeof spiner[keyof typeof spiner];

export type AsProps<E extends ElementType = ElementType> = {
    as?: E;
};

export type MergeProps<E extends ElementType> = AsProps &
    Omit<ComponentProps<E>, keyof AsProps>;

export type PolymorphicComponentProps<E extends ElementType, P> = P &
    MergeProps<E>;

export type PolymorphicComponent<P, D extends ElementType = "button"> = <
    E extends ElementType = D
>(
    props: PolymorphicComponentProps<E, P>
) => ReactElement | null;

interface SpinerProps {
    spiner?: Spiner;
}
export interface BaseButtonProps extends LayoutProps, SpaceProps, SpinerProps {
    as?: "a" | "button" | typeof Link;
    external?: boolean;
    isLoading?: boolean;
    scale?: Scale;
    variant?: Variant;
    disabled?: boolean;
    startIcon?: ReactNode;
    endIcon?: ReactNode;
    heightSpiner?: number;
    widthSpiner?: number;
    spinerColor?: Variant;
    size?: VariantFontSize;
    transparent?: boolean;
    children: ReactNode;
}

export type ButtonProps<P extends ElementType = "button"> =
    PolymorphicComponentProps<P, BaseButtonProps>;
