import styled, { DefaultTheme, css } from "styled-components";
import { space, layout, variant } from "styled-system";
import { scaleVariants } from "./theme";
import { BaseButtonProps } from "./types";

interface ThemedButtonProps extends BaseButtonProps {
    theme: DefaultTheme;
}

interface TransientButtonProps extends ThemedButtonProps {
    $isLoading?: boolean;
}

const getDisabledStyles = (props: TransientButtonProps) => {
    const { $isLoading, theme } = props;
    if ($isLoading === true) {
        return `
        &:disabled,
        &.button--disabled {
          cursor: not-allowed;
        }
      `;
    }

    return `
      &:disabled,
      &.button--disabled {
        background-color: ${theme.colors?.backgroundDisabled} !important;
        border-color: ${theme.colors?.backgroundDisabled};
        box-shadow: none;
        color: ${theme.colors?.textDisabled};
        cursor: not-allowed !important;
      }
    `;
};

const getOpacity = ({ $isLoading = false }: TransientButtonProps) => {
    return $isLoading ? ".5" : "1";
};

const getBackGround = (props: ThemedButtonProps) => {
    const { variant, theme } = props;
    if (variant) return theme.colors[variant];
    return theme.colors?.primary;
};

const getFontSize = ({ size, theme }: ThemedButtonProps) => {
    if (size) return theme.fonts[size];
    return theme.fonts?.md;
};

const Transparent = css`
    background-color: ${getBackGround};
    color: #fff;
`;

const Opacity = css`
    opacity: 0.65;
`;

const StyledButton = styled.button<BaseButtonProps>`
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0;
    text-align: center;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    line-height: 1.5;
    padding: 10px;
    font-size: ${getFontSize};
    position: relative;
    background-color: ${({ transparent }) =>
        transparent ? "transparent" : getBackGround};
    border-color: ${getBackGround};
    color: ${({ theme, transparent }) =>
        transparent ? getBackGround : theme.colors?.white};
    border-radius: 4px;
    outline: none;
    text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1);

    transition: all 0.5s cubic-bezier(0.165, 0.84, 0.44, 1);
    opacity: ${getOpacity};

    div {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    &:hover:not(:disabled):not(.button--disabled):not(.button--disabled):not(:active) {
        ${({ transparent }) => (transparent ? Transparent : Opacity)}
    }

    &:active:not(:disabled):not(.button--disabled):not(.button--disabled) {
        opacity: 0.85;
        transform: translateY(1px);
        box-shadow: none;
    }

    ${getDisabledStyles}

    ${variant({
        prop: "scale",
        variants: scaleVariants,
    })}

  ${layout}
  ${space}
`;

export default StyledButton;
