import React from "react";
import Button from "./button";
import { scales, spiner, ButtonProps } from "./types";
import { baseColors } from "../../theme/colors";
import { baseFonts } from "../../theme/fonts";
import { Grid, Box, Flex } from "../Box";
import { Text } from "../Text";
import { Meta, ArgTypes } from "@storybook/react";

export default {
    title: "StyleGuide/Button",
    component: Button,
} as Meta<ButtonProps>;

const colors = Object.keys(baseColors) as (keyof typeof baseColors)[];
const spiners = Object.values(spiner);

export const Buttons = () => {
    return (
        <Grid gridTemplateColumns="repeat(5, 1fr)" gridGap="10px" mb="32px">
            {colors.map((item) => {
                if (item !== "white") {
                    return (
                        <Button key={item} variant={item}>
                            {item}
                        </Button>
                    );
                }
            })}
        </Grid>
    );
};

export const Transparent = () => {
    return (
        <Grid gridTemplateColumns="repeat(5, 1fr)" gridGap="10px" mb="32px">
            {colors.map((item) => {
                if (item !== "white") {
                    return (
                        <Button transparent key={item} variant={item}>
                            {item}
                        </Button>
                    );
                }
            })}
        </Grid>
    );
};

export const Loading = () => {
    return (
        <Grid gridTemplateColumns="repeat(5, 1fr)" gridGap="10px" mb="32px">
            {spiners.map((item) => {
                return (
                    <Box>
                        <Button
                            key={item}
                            isLoading
                            spiner={item}
                            mr="10px"
                            width="100%"
                        ></Button>
                        <Text>{item}</Text>
                    </Box>
                );
            })}
        </Grid>
    );
};

export const Scale = () => {
    return (
        <Flex alignItems="center">
            {Object.values(scales).map((scale) => {
                return (
                    <Button key={scale} scale={scale} mr="8px">
                        {`${scale.toUpperCase()}`}
                    </Button>
                );
            })}
        </Flex>
    );
};

export const Disabled = () => {
    return (
        <Button mr="8px" disabled size="xss">
            Disabled
        </Button>
    );
};

// export const Primary = {} as ArgTypes;

// Primary.argTypes = {
//     variant: {
//         options: Object.keys(baseColors).filter((item) => item !== "white"),
//         control: { type: "select" },
//     },

//     size: {
//         options: Object.keys(baseFonts),
//         control: { type: "select" },
//     },

//     scale: {
//         options: Object.values(scales),
//         control: { type: "inline-radio" },
//     },

//     isLoading: {
//         options: [false, true],
//         control: { type: "inline-radio" },
//     },

//     disabled: {
//         options: [false, true],
//         control: { type: "inline-radio" },
//     },

//     spiner: {
//         options: Object.values(spiner),
//         control: { type: "select" },
//     },
// };
