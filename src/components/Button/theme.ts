import { scales } from "./types";

export const scaleVariants = {
    [scales.MD]: {
        height: "48px",
        padding: "12px 24px",
    },
    [scales.SM]: {
        height: "32px",
        padding: "8px 16px",
    },
    [scales.XS]: {
        height: "20px",
        fontSize: "12px",
        padding: "4px 8px",
    },
};
