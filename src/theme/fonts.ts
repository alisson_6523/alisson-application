import { Fonts } from "./types";

export const baseFonts = {
    xl: "2.875rem", // 46px
    lg: "2rem", // 32px
    mdl: "1.5rem", // 20px
    md: "1rem", // 16px
    sm: "0.9375rem", // 15px
    xs: "0.875rem", // 14px
    xss: "0.75rem",
};

export const variant = {
    XL: "xl",
    LG: "lg",
    MDL: "mdl",
    MD: "md",
    SM: "sm",
    XS: "xs",
    XSS: "xss",
} as const;

export type VariantFontSize = typeof variant[keyof typeof variant];

export const fonts: Fonts = {
    ...baseFonts,
};
