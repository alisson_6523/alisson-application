import { DefaultTheme } from "styled-components";
import { darkColors } from "./colors";
import { fonts } from "./fonts";
import { styleGuide } from "./styledGuide";

const darkTheme: DefaultTheme = {
    siteWidth: 1200,
    isDark: true,
    colors: darkColors,
    fonts,
    styledGuide: styleGuide,
};

export default darkTheme;
