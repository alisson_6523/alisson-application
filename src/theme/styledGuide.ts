export const fonts = {
    xl: "2.875rem", // 46px
    lg: "2rem", // 32px
    mdl: "1.5rem", // 20px
    md: "1rem", // 16px
    sm: "0.9375rem", // 15px
    xs: "0.875rem", // 14px
    xss: "0.75rem",
};

export const styleGuide = {
    h1: {
        fontFamily: "Lexend",
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: "120%",
        letterSpacing: "0",
        fontSize: fonts.xl,
        textTransform: "uppercase",
    },
    h2: {
        fontFamily: "Inter",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "130%",
        letterSpacing: "0",
        fontSize: fonts.md,
    },
    h3: {
        fontFamily: "Lexend",
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: "120%",
        letterSpacing: "0px",
        fontSize: fonts.lg,
    },
    h4: {
        fontFamily: "Lexend",
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: "30px",
        letterSpacing: "0",
        fontSize: fonts.mdl,
    },
    h5: {
        fontFamily: "Lexend",
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: "120%",
        letterSpacing: "0.02em",
        fontSize: fonts.mdl,
    },
    h6: {
        fontFamily: "inherit",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "130%",
        letterSpacing: "0.02em",
        fontSize: fonts.md,
    },
    p: {
        fontFamily: "Inter",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "140%",
        letterSpacing: "0.02em",
        fontSize: fonts.md,
    },

    span: {
        fontFamily: "Inter",
        fontStyle: "oblique",
        fontWeight: "700",
        lineHeight: "140%",
        letterSpacing: "0px",
        fontSize: fonts.xs,
    },
};
