import { Colors, Fonts, StyleTags } from "./types";

export interface Theme {
    siteWidth: number;
    isDark: boolean;
    colors: Colors;
    fonts: Fonts;
    styledGuide: StyleTags;
}

export { default as dark } from "./dark";
export { default as light } from "./light";

export { lightColors } from "./colors";
export { darkColors } from "./colors";
export { fonts } from "./fonts";
