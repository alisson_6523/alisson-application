import { DefaultTheme } from "styled-components";
import { lightColors } from "./colors";
import { fonts } from "./fonts";
import { styleGuide } from "./styledGuide";

const lightTheme: DefaultTheme = {
    siteWidth: 1200,
    isDark: false,
    colors: lightColors,
    fonts: fonts,
    styledGuide: styleGuide,
};

export default lightTheme;
