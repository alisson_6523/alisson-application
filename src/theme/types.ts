export type Breakpoints = string[];

export type MediaQueries = {
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
    nav: string;
};

export type Spacing = number[];

export type Radii = {
    small: string;
    default: string;
    card: string;
    circle: string;
};

export type Shadows = {
    level1: string;
    active: string;
    success: string;
    warning: string;
    focus: string;
    inset: string;
};

export type Gradients = {
    bubblegum: string;
    inverseBubblegum: string;
    cardHeader: string;
    blue: string;
    violet: string;
    violetAlt: string;
    gold: string;
};

export type Colors = {
    white: string;
    black: string;
    primary: string;
    primaryBright: string;
    primaryDark: string;
    secondary: string;
    secondarybright: string;
    secondarydark: string;
    tertiary: string;
    tertiarybright: string;
    tertiarydark: string;
    success: string;
    failure: string;
    warning: string;
    cardBorder: string;
    contrast: string;
    dropdown: string;
    dropdownDeep: string;
    invertedContrast: string;
    input: string;
    inputSecondary: string;
    background: string;
    backgroundDisabled: string;
    backgroundAlt: string;
    text: string;
    textDisabled: string;
    textSubtle: string;
    disabled: string;

    // Gradients
    gradients: Gradients;
};

export type Fonts = {
    xl?: string;
    lg?: string;
    mdl?: string;
    md?: string;
    sm?: string;
    xs?: string;
    xss?: string;
};

export type ZIndices = {
    dropdown: number;
    modal: number;
};

export type StyleTags = {
    h1: StyledGuide;
    h2: StyledGuide;
    h3: StyledGuide;
    h4: StyledGuide;
    h5: StyledGuide;
    h6: StyledGuide;
    p: StyledGuide;
    span: StyledGuide;
};

export type StyledGuide = {
    fontFamily: string;
    fontStyle: string;
    fontWeight: string;
    lineHeight: string;
    letterSpacing: string;
    fontSize: string;
    textTransform?: string;
};
