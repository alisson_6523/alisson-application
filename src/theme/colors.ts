import { Colors } from "./types";

export const variant = {
    WHITE: "white",
    BLACK: "black",
    PRIMARY: "primary",
    PRIMARYBRIGHT: "primaryBright",
    PRIMARYDARK: "primaryDark",
    SECONDARY: "secondary",
    SECONDARYBRIGHT: "secondarybright",
    SECONDARYDARK: "secondarydark",
    TERTIARY: "tertiary",
    TERTIARYBRIGHT: "tertiarybright",
    TERTIARYDARK: "tertiarydark",
    FAILURE: "failure",
    SUCCESS: "success",
    WARNING: "warning",
    CONTRAST: "contrast",
} as const;

export const baseColors = {
    white: "#FFFFFF",
    black: "#000000",
    failure: "red",
    primary: "#696969",
    primaryBright: "#74A798",
    primaryDark: "#1d343e",
    secondary: "#333333",
    secondarybright: "#F4F4F4",
    secondarydark: "#808080",
    tertiary: "#00FFB2",
    tertiarybright: "#FFF5E6",
    tertiarydark: "#C79E00",
    success: "#31D0AA",
    warning: "#FFB237",
    contrast: "#fff",
};

export const dev = {
    white: "#FFFFFF",
    black: "#000",

    primary: "#003456",
    primaryBright: "#0A7AC4",
    primaryDark: "#0261A0",

    secondary: " #E6E6E6",
    secondarybright: "#F4F4F4",
    secondarydark: "#808080",

    tertiary: "#00FFD1",
    tertiarybright: "#2E5772",
    input: "#75747D",

    backgroundAlt: "#04A8DC",
    textSubtle: "#616164",
};

export type Variant = typeof variant[keyof typeof variant];

export const lightColors: Colors = {
    ...baseColors,
    ...dev,
    background: "#FAF9FA",
    backgroundDisabled: "#E9EAEB",
    cardBorder: "#E7E3EB",
    dropdown: "#F6F6F6",
    dropdownDeep: "#EEEEEE",
    invertedContrast: "#FFFFFF",
    inputSecondary: "#d7caec",
    text: "#280D5F",
    textDisabled: "#BDC2C4",

    disabled: "#E9EAEB",
    gradients: {
        bubblegum: "linear-gradient(139.73deg, #E5FDFF 0%, #F3EFFF 100%)",
        inverseBubblegum:
            "linear-gradient(139.73deg, #F3EFFF 0%, #E5FDFF 100%)",
        cardHeader: "linear-gradient(111.68deg, #F2ECF2 0%, #E8F2F6 100%)",
        blue: "linear-gradient(180deg, #A7E8F1 0%, #94E1F2 100%)",
        violet: "linear-gradient(180deg, #E2C9FB 0%, #CDB8FA 100%)",
        violetAlt: "linear-gradient(180deg, #CBD7EF 0%, #9A9FD0 100%)",
        gold: "linear-gradient(180deg, #FFD800 0%, #FDAB32 100%)",
    },
};

export const darkColors: Colors = {
    ...baseColors,
    ...dev,
    background: "#08060B",
    backgroundDisabled: "#3c3742",
    backgroundAlt: "#27262c",
    cardBorder: "#383241",
    contrast: "#FFFFFF",
    dropdown: "#1E1D20",
    dropdownDeep: "#100C18",
    invertedContrast: "#191326",
    input: "#372F47",
    inputSecondary: "#262130",
    primaryDark: "#0098A1",
    tertiary: "#00FFD1",
    text: "#F4EEFF",
    textDisabled: "#666171",
    textSubtle: "#B8ADD2",
    disabled: "#524B63",
    gradients: {
        bubblegum: "linear-gradient(139.73deg, #313D5C 0%, #3D2A54 100%)",
        inverseBubblegum:
            "linear-gradient(139.73deg, #3D2A54 0%, #313D5C 100%)",
        cardHeader: "linear-gradient(166.77deg, #3B4155 0%, #3A3045 100%)",
        blue: "linear-gradient(180deg, #00707F 0%, #19778C 100%)",
        violet: "linear-gradient(180deg, #6C4999 0%, #6D4DB2 100%)",
        violetAlt: "linear-gradient(180deg, #434575 0%, #66578D 100%)",
        gold: "linear-gradient(180deg, #FFD800 0%, #FDAB32 100%)",
    },
};
